// Import react
import React from 'react';
import { slide as Menu } from 'react-burger-menu'

class App extends React.Component {
    render() {
        return (
            <div id="main-container">
                <header>
                    <Header/>
                    <BurgerMenu outerContainerId={ "main-container" } />
                </header>
                
                <main id="page-wrap" className="page-wrap pb-5">

                    <Breadcrumbs/>
                    <div className="container pl-4 pr-4">
                        <h1 className="mt-4 mb-4">Založit nový lead</h1>
                        <LeadForm/>
                    </div>
                </main>
            </div>
        );
    }
}
export default App;

class BurgerMenu extends React.Component {
    showSettings (event) {
      event.preventDefault();
    }
  
    render () {
      return (
        <Menu right >
          <a id="home" className="menu-item" href="/">Home</a>
          <a id="about" className="menu-item" href="/about">About</a>
          <a id="contact" className="menu-item" href="/contact">Contact</a>
        </Menu>
      );
    }
}

class Header extends React.Component {
    render () {
        return (
            <div className="vf-nav-strip cf">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-6 col-md-4">
                            <MainMenu/> 
                        </div>
                        <div className="col-lg-6 col-md-8">
                            <div className="row">
                                <div className="col-lg-5 col-6">
                                    <Profile/>
                                </div>
                                <div className="col-lg-7 col-6 icons mt-2 mb-2 pl-5">
                                   <div className="row">
                                        <div className="col-2 mt-2 text-center">
                                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="home" className="svg-inline--fa fa-home fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M280.37 148.26L96 300.11V464a16 16 0 0 0 16 16l112.06-.29a16 16 0 0 0 15.92-16V368a16 16 0 0 1 16-16h64a16 16 0 0 1 16 16v95.64a16 16 0 0 0 16 16.05L464 480a16 16 0 0 0 16-16V300L295.67 148.26a12.19 12.19 0 0 0-15.3 0zM571.6 251.47L488 182.56V44.05a12 12 0 0 0-12-12h-56a12 12 0 0 0-12 12v72.61L318.47 43a48 48 0 0 0-61 0L4.34 251.47a12 12 0 0 0-1.6 16.9l25.5 31A12 12 0 0 0 45.15 301l235.22-193.74a12.19 12.19 0 0 1 15.3 0L530.9 301a12 12 0 0 0 16.9-1.6l25.5-31a12 12 0 0 0-1.7-16.93z"></path></svg>
                                            </div>
                                        <div className="col-2 mt-2 text-center bell">
                                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="bell" className="svg-inline--fa fa-bell fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M224 512c35.32 0 63.97-28.65 63.97-64H160.03c0 35.35 28.65 64 63.97 64zm215.39-149.71c-19.32-20.76-55.47-51.99-55.47-154.29 0-77.7-54.48-139.9-127.94-155.16V32c0-17.67-14.32-32-31.98-32s-31.98 14.33-31.98 32v20.84C118.56 68.1 64.08 130.3 64.08 208c0 102.3-36.15 133.53-55.47 154.29-6 6.45-8.66 14.16-8.61 21.71.11 16.4 12.98 32 32.1 32h383.8c19.12 0 32-15.6 32.1-32 .05-7.55-2.61-15.27-8.61-21.71z"></path></svg>
                                        </div>
                                   </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                </div>
            </div>
        );
    }
}

class Profile extends React.Component {
    render () {
        return (
            <div className="card">
                
                <div className="card-body">
                    <img className="card-img-top mr-2" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD//gA7Q1JFQVRPUjogZ2QtanBlZyB2MS4wICh1c2luZyBJSkcgSlBFRyB2ODApLCBxdWFsaXR5ID0gOTAK/9sAQwADAgIDAgIDAwMDBAMDBAUIBQUEBAUKBwcGCAwKDAwLCgsLDQ4SEA0OEQ4LCxAWEBETFBUVFQwPFxgWFBgSFBUU/9sAQwEDBAQFBAUJBQUJFA0LDRQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQU/8AAEQgAyADIAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A+1g4aoZuh9ahhm9TUzuGHWugyK4lw3NW4ZgaoMMtxzVm3iHWgC+mJKd5QNRxSBTg8VZG0rnNACQTeU2O1XhcjbmsS7l8o5Bqg2rMBgnFAG1e3qpnkVh3WqqDzwazb/UWJJ3ZrnNT1YEkhunagDs7fUwzZByKvpeZwegry+18SCOUAvx9a1j4pX5QH5NAHocN8CwGecU+W53N6Vxdh4gVnAJyT6V0dqxuQGJ60AXTdHcOamg1EBgM/rVcxbDniq727vIClAHRR3Qdev4VDPMHBweaq2ysFw1TOmxc4oArls1E/T3omO3JHBqlJd8gDrQBOzbR1qCdvl61HLISetUry6KHr2oAiu5cMahhAPzZqjeamMHmq9vqangtigDZkTKk5orEudcRUKo4JooA61Ixjrk0N8oxmsyHVUxktgUf2nHK/DgigC+JNpwanjnEYyx4rLluowM7s/jWZe6pt4VuKAOhm1WNTw1EGqB+S2BXnt9rhifOeKZb+IS54bGaAO/vNQTaTurEvdTQJgcntiuX1TxLBYQSTXVwsMMYy8jsFVB6kngV8cftA/tU6pey3Gk+GZ1t9LWZoHubYM0rkAEEyDhQSTwB25NA0rn1Z4l+K2jaIXS71OyjdW2sjXSBlPoeeD9a4nxP8dfC0WneZYa5ZPPMxjhknD+QX9N4GG/4Ca/PG78VXhuHW5t7Z7kNkuLlvNBPQ53Efhirs/izWLO3MNwou7W6TBiudsvr91ujDHXv6jgUrofKfckHxYurFd88Gma3Gg3SR6dNJFcnPQRq4Ifp0ByfxGetsfiHpetQxzWgnZpADHGoLuwIyCAB74r88oPHMMhswyNbhPlO1zvTHAAJ64PTPbA9a7TSPiNren3cd8rSySAiUEnG8bsHp3Oee+cHuaHKKDlZ+gHhDxdpmo3qRJfAXHe3lUxyqfQqeQeD+Rr1ixv/ADIlKHgd6/ODVPj2b3xHp+sRxNb3toBIkgzvUdGUnvjnH+zxX0N4C/aasdR0yaCSaCGW2LNJPcy7FA3vnLc56ADjtTunsLlaPpy98SQxHY7Yan6fr8UzDmvKPD3jjRfHkUkdhqMM17FH5kkCOGdFzgEgHirlpeXFpLgfMAcUEXPcba6jlUc1bZFZOvFcL4YvZblVLH8K7CK4xGdxoKKt5bPu4biofsYxzikm1NY5yrc0yTVI9h5FAEc9uACc1hamDg7O1WbnV13ldwrOu75DESGGaAOcuhI0hLHj0qnO2EODgCn6nqKockgVhy6sjMVDA0ANu7plJCtmimKguXOGooA6C4uJnXIcj6UW5mAB8zJPaqdtLLeMFQDBrfsNOIGDy1AFOeW6WPO4bazjeSTjaCSa6W4sDCuGwSe1QWumIMycCgDm5rKdhuKZ+tZl9HNaJvJ2k859AOprubhkiBJ5X0rxv4/fES28HeB9VuUuIor+W2eC1jaVVfe3G9QTzt68elAHzD+0X8cJ/EOqX/hqCWW18LwHy3uYD+9uZlzkqe6ZOOeDjPJxj53SPzYGi0/zvJl+UyMxaNe+OAPm/D3x0qfXNWu9qx+fLNIFWOM7wx2jgLwcYGOn0rMvbm+0q1+wyK1s0mJihYgFiPvkeuKzbNFsR6npc6KrEqSE2gx4zjryM/WodJmux+6KSSRd025FdD4W8HXmvMgCNsPG7BFfV3wT+A+ixPDLqsCTO2NqygEflWE6iiaxhKWx8vr4IudXhS7gtZ/KbgsiEbD2P0r6M/Zu+HVj45jtNH1m3a3vIpGUzBRkqRx9enB+tfX2k+A9CtraO2jsbZYuBgRr/hXR+H/COi6Hqa3Nvaxwzqf4RjNZuXMtTZQSPK9V/Yj0W5lS5gUGZcllHAPr+fP6V4F8SP2SPFXh19Qn0gGe1yjOiZ3tjnA/XpX6Hf24FCbSCehwatW/k3cQaUIwbswpWe6Y2kflV8MfG2sfB3Wd08N7HLDIz3MJTJcHO5duMtwFAAIGQDX294T8RReI9KsNRSM24vIFmMLkFoyeqtjuDkfhUf7WfwBsPFngS51nSLZY9W08mbMa4Z0z8wyOeBXi37LPiltY8PX2hXDTS3umuJPtAIKOjE/LnqrKQfl9CMV1U530lucdSNtUfXvh+YQxxsp5Fa2pan8g2khj6V5tpmr3MWEJOBxkVu2d1Lckbjn61sZk819LNKRuIq1DDI6D5jUlpZq75IrWjiRIyD09qAOZvLZlYtWbc3QAwTXQ6vhFbbXG30v7wAdaAMfWoJbliUPHYVjRaXKs2WB/Gu6ttP8AtGCR+BrW/wCEZScKWXGBQBxenWZjYMRnPaiuxk0aODA2gAd6KAOE0XU90iYruLW82IH3gGvIdOFzBgh62l1W7RMFmoA7HXdddhhCOO9Yj+MCoCBssTg1zV/qdzOrRoTu9axG0y9D+aC2aAPTbjXsWeTjcw4zXxn+2h4g8rV9BtGWCZYbeS52ybWw7NtHyn/d78d+eK+k7W5mZE84nK8ENXxf+1EJL34uau8jmURxW6W8TDAUmNc546Dk55PzYFJjW545Z3UtpcRXt9i8BOVjkUbQMHgYPHWtTw9pT+PvGNs020O5VcckYGAOPyrKvEt0hNukyPcA5ZthBf6ZJ9/Suh+GepxaTq0chyrhgS54K/8A16xm3Y2W59VaB8PrPQrJI44wZY1wWPr3rc0vXrnS2BjRyinGABn6VS0/X7XV7O3NvNIS0fKuBknHU11uh2+nu5+1KnnqoJzjBz0/rXnO7ep6MdFoei+ANfk8QTIgEpKgEnb068V6NPpd0XVo13AA9Tk59a8h8MeOdK8MarGzKEjLBN6cAfWvdbHxTZTWCXMTbx1ZQAcCrjtYmW4mlxichZRkYx05z37V11lpQWFArYBOa821DxzY2V1K4dQp+ZcdD7fpXXeGPE8WvWyPZTxyZ5O1gf8AP/1q1T6EtM6DU9P+1abeWMiZSWNkJJ9q/Nzwvo918Kv2o0sUlWCw1S7a0cySlVwcuRsAO44Tg8AZ/Gv0rjnWYqkp2sxK8/TP9K+Mv2h/ASab+0r4BvPIEi6ldK0bM+0LcIcK34blbjrjHOauLtJHNJXR79baKqpkrwO4q2LZLUZGeK7K08OT+VsEe1GHH5VDdeCrmdiBgA12J3OWxzMN58wArUgDy9ia2NP+HMofMjGuu0zwdFbgBlyfemI8yvNMkuRwprOk8GyzyAhDnPpXuieFof8AnmOPap4/DsajhAPwoA8X07wdeJKMoSvriuqs/CMrqC9ejx6Gq9cVci0tEGMcfSlcDyTWPCDeUdiZYDsKK9ck0uJl+7n8KKLhZnxBpunqAMjn3rQbTllOMcUyVhAuRxUltqKgDPX3pgOi0GMvnA/KtaHw/HKgGwVBa36Mw/i+lbdjJK5BSNj+GaAMO68IRnov6V8Efth6XPpnxguYwXiifToHjAYgP98M7dsDaR+VfpdFp9xdDAgbn1FfD/8AwUQ8HSWfibwffSRmL7XbTW7SKuc+W6sAfp5pNZyepaR8UaisMcCxiXfKGJc7SpP9MfrWh4JtJLnU18okgEEsR0rLu7kPNJFGqlV4y/fH8q9Y+C2gjUbGWZUJcvwQM8VlN6GkVdnuPwg0AX0Mks8hWKFQd7Dj2616Rfapo3hzSlbUdSgtXOZJDnCqfT3wMD8K8O8a+K5/CXhkafZyNbXEjgyMTnIA4/WvBNZ8TC4vUkvbqfVrs8JCGIUD0PoK5eXmOzn5VZH0l4o/aO8I6WWtdJsJPEV4flLODHEvbIGMtW98PP2lZo7+Bb9jChicPC3yqDwR/UV8n2Fn4i8Qo0VrZfYYC/zLAgUbfXfuDE9O1XYfB+s6fcWxleRHc/NCZPMIA6nI7VTglsZqcmz7p8S3uoeIvhqNW01TM6uj4HGPm/lg/pXznr/xX1/wg32JNea0uXcu6QBmZPQnb0xk+lfav7POjWHij4HtpduFW4aEozYO4Pjg18w+Of2WrqHxBMiwT3EchKSJIqgEt1O4nnrx9PxrNaO5vK7Vin8Lv2kPE2qaxBZTePrOd2YbEufMTdjnBJHt1yTX0t8V9E1n4ieEvh9rBktpNb0bxFahpkzs+ZuG78ZCdfxryP4RfsX6beX0Sa5ot3LaI2cyzspB55DKcjHBFfYdj8PIfAvw+urfTla4j04RXkUcpLk+Q6vtyeSSqEfjWt9brY55K0bPc9p8P2bLpiJMF8xOrK24MfXPetKO1jLYwDXEeGfilZeNvCNvrunRvFbTF1QOu0naxXOOwOM1Y0XxY91e7Gbj0rdTXQ5nGzsz0CCxj4O2rSWoHaobGfzIwc5q6DWlxWGCIU7yxTqM/SgBPLFAXmlLik3igYuKKaJBRQB8zW3wdMx/fyOR6dK2Lb4O2KABo931r18WShuwp/2NM9qbkTY870/4b2NoRtt0+pFb1t4VtrdRiJR9FrqhbIvbmgxL6UudhYwxpUSL8sYBHtXzH+3l8L7fxz8LNNaPU9O0nVbC9zbS6lJ5ayq6FZFBAJzjB4H8NfRHxF+I2hfDfTBeazdC1RjtQd2bsB+n518c/wDBQO+svHvw18J67pOpQ6rZWtxcIxt5FYAugwWA6EeUwqJyaVzSEVKSTPzc8e/D7WPBOr/Yr+GHbICYrm2YSRSj1Vh/XBr6L/ZP8Mmbw/NczKQucBTx7j9K8e1iRdPmtoLu5e+tmZVEYBJT8T7cfjX0D4F8SW3h7TUgtIXgZ3YtHu+UfMef8+1Yyk3HU2jHlkdJ43+Dtp4vuHXILqu0Yrzhv2W5bG6VlZTGzclBljXoZ+IK2F2pLlipJPJOSciuh0T4m2l9fC2ecKvG0/3jxwK5uZo61FNalLwP8CtK0iENLYzS3JGTJcudo+ijg/jWF8QfCNl4fmaZY0Vtoy+AAvHAGPxr6JsZodStFcnPAyccj3rwv4/X+naJ4h0T+1t76e5eV0XrIyj5R+v6VbbYWR6t+yJ4kgsIHgzlZSQB719M69omnXEAaeKN0nBADDqf8a/LTQv2oY9K8cJFpFv9kso5dgj2kbj069M19ueG/jtf+JBomif2FqN3BqduZl1NYMQQOPujf1LcE8Dp9aafLoyLc2x6hp+kx6GzSW7v5AOdmeldbaaisimNiHR/kYHuDXHaJdzXdq8VypV8YdcdDWrGvlzggkKcHPoe9a9CJI8q8HDXraWTw74c8Q6bFBYO8R0y6sDIyOHyWeQSA4YHjhR+Wa9d8FJdSbF1WxXTtTBIeNG3I2P4lPoev/16/PHxh8QG+F/7X8/ihppYAt0BdQx8RzW5Uj5hnrjb27Zr7u0D4v6T8TPB0Op+F2eS4YrLC8qFVibcNwYnjoDmhJJJnLK7bZ6lqnj+w8Mtb2s0oa7mJCQIcuQASTjsAByenI9RXU2er+dGCxAcjJAPC+1fH3xA0qXRfih4dvI7mee1kkCzzyTfPcksrhfmI/iQHPIAzgcgD6M8OXcUmliaecAKoZkCGNF/76AJ6jkjmt07q6IO/GoIf4h+dI2oqB1riovGOjO1uI71JnuJDGkcR3Ekdenp3rq7SCO6jDKeD2zTAc2q88DNM/tF3OApq0umIDzUgs0U9KQFVbiQnOKKvCBM9KKAMZpvm605Zx61hveOxznAponc8BjUcwHQG5UDqKQ3SgElgB61z5kbqW49a8r+JX7SPhP4ds9nJfQ6nqpGBZ20gYqTwN5GQozj356UJt6JAeUft/XOkeLPCcOl2GsQL4gtZFuooWcYTb/EfTtx3IHHFfm9F45vNF8ZXC3hddNvo0F1aKxMYII3bR0GMNj0Bx0r6q/a+8aDWZNPu7hBY6hdQfuraFigCt/eLYJBB6kDlRxXxp4o8IajDeCS4OxIkXLvIMkn+Ff72M84zRzXfKPbU9gvPAEUlppur6XMdRvI5fMkt4+QHjOefYgA/QjrVaTxOxuIpJYWspJQyzLJHtKurEcjrXLfC3xlpOq6jp2k+KJjpzW8LRR6hC5XzQDmNHPQ46A+iipviVrOkf8ACUPZ6NNLJEkYkM00m5pH7/TsMegrNqzszocrq6OrtNXWSQswVuq5I7etb/hssb5GReAQVYHPHrivO9LuFkgjAJacdQOenX6Cu60W6VBEjsQ6qN0Y78//AKq52rOyNlI+ofh7r222htpmycYyDisD9ozwhpHivw5I995cS2w82OYPtZSBk7T6+1cr4d8THSrU3MrL5MS+Zlum0CvIPHXxauPH19I84MOl2zHcXOFIyP8ACkr30NHJJGV8H/A9v4y8aR6fJbRXAKs6QzvsZiB13e3WvpTV774l/CvU9Mvok83S4pliW0tnDkJtPLoP4enNfOnw/svFGr3XneD/AA+1+gG1Jp2WMY6HBbk9u2K+gPDvgz446lFbWdwmi6VCI8N9pneWRx2BIGBWj1Y4w90+rPh/4+03x7pVrfIq2GothJbc8AsADx+ddN4hu49I0t7iQhSn6nsPxr48TTfiN4J1W1j1PTTFaWjLK+oWrB4Juex4IIGAcgdD9a9v+I3jWa88E6PcSMUju4fNnKZypUZP05HekpvVMxkuVnwZ8aLOP4h/tDyWcKP5kwCLDs7n7pcj+EFgST0AI9M/Z3w58R6P8M9AsdBgXzbiGFGZVjVUXOWYBeiksG46L27V8h6X4vhh8WatrUEZbUNqKLh1wpTLA5OOoPP49q07rxde+JGulubk2trCpfz1O1nY8Ng57EfTkilUk7WRikmz2/x18TPGvxG+JVtY+Ere1lNvGHa98lWisl3EBjIVY7jj7oHIznpx6n4jtfFlj4Au0l1ZbvVPJdkEM2yKPKlTI7OTyByDjrjjgY80+CGo2mj+HzBCn2eKUhzI7nfI2AN5JOe2O2McV3fiPxxpiWM2myX7O8sf7y3tiWdlx34yB9fWs41pLQhwPmD4ffEPXoviUkNtqmVsjI1zqGqXIiWXc5y6jgRoMpgLzlj1zx+i37Pnxd0nx9oappd4lzFbxrubJJC4+XJPJJG0kkDkmvyW+J/hq7tPGmp3UVr9mhn3JZWcnDgEYDYIyccnceuBjI6/Yn7LPjzRPg58IbwX9zAdeZ44secrSXNwY18uHrnCjYuOi7W54OPUUrxMrWP0DTWIJXdVZTsbYTnvjmmy6pGnO4fnXzn4P+JiW3hq0uprtZZtRlZbRi3yyKBl5gf7gOSW9hjk4HpularJqIVzuAH9/jA9W9z1x24z6VDYHcNrSHPIorn1+fkHGaKXMwIUU7ehNV9W1Wz0HTri/v5ktbSBDJJLKwAVR3qxd3dvptnNc3M0cNvAhkklkYBUUDJJJ6ADvXyJ8TvHmrfG2+k0/T3/ALO8MyyGK1iaULJcID81w4PIzg7FI5HPes/NgUvit8WNY+Lem6hfWerP4c8A2xeK3tYZDFeatIOCXPVI89FHJyPw+Z/CVnbXvie31aSCabTLGXz/ALHFvdp2B4yRgIuemSdxzwea9b8beF5LbU7DwzFJCivGI5bqIgNBFtyETAJAYDrx1rl9V1e0+HEcFna6nJY21ijPcJA+JyrcEIMEK2P4j83+0OFo52lZdR21PJfjN4oufHnxDW91Gymso4usDyZZRkAKwPCk88dvTtXEa7PcPojW1rcGS2VvNVI2bBXGBgnkY44z9K9Rv7WxvfDl1q+i+GruDSzIpS8nLSyBh99sZLKucjcf0PA566Nn4U0+HWLNGdYj5ckF9GHUSbAT8px1Bz9COlKnZblM+d9WsplYTP5qiRiy8dT9elVUuVCLIrBZ0bJ7bh/kV6d8UvFll4wkS4NhBa3TcmWOELvGOgIxjHtXk11aPbzY27VPKlTkY+tdG5B6T4Q18RqrBhk/K2D0/wA4rr7fxE8t350Mgyw5JxgDr09sV4vZSz6e8cy52962LHXhHcqATsLdC3A96xcDVSsfS8Hiu11DwxdWQBkuZV2KFAJA6dvxPFdB4Y+Hmm2fgGJL6yjvriYrKwlXJBGcYr548J+K5NLlNzyxDdF6+g/lX1P8OviBour21vbX9ypd9gCMwySQc59OR+tc04tbHRCae5V0Ge78K3RfRbSaPdHhdvKhuOMV6D4e8QfFzVdVtXubALZkA5EZzj1//XXSad4x8IaDAJoLiCYFS6ncucD1/GtXQ/j7GktyYxHKIpWRVTpJtAOR9Sf6VKTtdm/P5nr3h6W7vtLEOrQ743Ub1YZ28c181ftQeOl8I31vp2nziGBLOV4kVsEYycZPTp/PtXsut/tDaJ/wjU02YopvLwFBBJJ4GK+APjt4/HirxI4QTTMzCMSM+WEZyCfTv+I69RWsY9Wcs5GKdTm0u1lMkJ2XbuyHIIBDZKn3BJ/MVDYMb0SpfySJGYxtRTw+c8fyqLSLOWUpp1xKs1usZuEz1Zhjj9Bz6V0eg6RZmFZJ32yMBHkkfKMHJ69SOPxqmluZJnQf8JRf/YwYNTFlC4Gdr4WNB9eRgc9zyKkv/H154a8KandaOs7O+1Eu593zHIHL9Djk4BAHfoK4nx/4mjttOh022t08sFvKUddx25dj/wABGAPX61ylhZahNp0clxMq2jkBFb5dy9OnORj1/KpVNbjcmaHwy1O/l8SSarfPBPdsWdZr0h18zqCfbOOvFdp8HPh9qXjj40QWtxqx1C0kcTX17FNgcyANk54Poa8ws7hrHUJEm2JHGrFGiAf3yvbP8vSut+CfxDfwbJqlzpl7HFdOoVLCWDDzEsAQrD6nPritZOSTaJST3P0P0D4e3VxrNrrVu7w2cNnHa6daKiiKwt1GFwG+9IeG9AeTnpXuHh2a3itYI4y0s2AoCnftx33HqfU4P9K+ffhV8WNSur3R9F1C3KzaospJ8zdhEVMuceuSOO7VvfFjxndeHprb+y9dht55GAWzhhae4mAHCpEgwB0JYjI/GphUjJWZMotH0nZKroDuDHv82f1or5P+G/xt8RSa2lpq0trp8IbckssLyzMM8gRqcL16844oro5F3IuV/jF8TZ/jR4jXwhoN3Gvgu2mA1bUkLBLxlOTCrZG5BwWIwDwM1x3xL8R2ngMWtl4ZhjtXl2ZnhT9668YSLPbALE4wOPXFUdE1ay0awutSRbGXTLNjGuHUW6FTwkca/eGSB/tFsknrWHHrFl4q1ay17UJZr++vmeMW8LYhhgU5+dQRkkjOCcYCjHc4yd3psMwfBN7qn/CWXviDyTJcTHyIbd5SRMXA4z1YAdevTtnNQeIvhxrHjfxQ813ZRRWbKHuJY18u2CgjO3BweHOOuSc812UGnW9xZjUhLOba5uiJTYpsURKR8pY8HsMYCjqcniuz8VX0nivw/rEcOqSyMlvvvY4XEJ8tlX91BkYjGflAxn5gTknFZybvdFLzPKI/AiEQWllqbC3CFplhuvLiSNSCxZsdznjHVhweKxPHOhaTr9jd2mra5b6eY1SGBZbWR2uo0j4kD7iWbaSOckgjvwKuu6zpNleWIhjlFrLIY3tGfbsiU7UD4HzAiNefVT0zVvV7ay+LGhq8l9a2d/poS3aFEVXeFSSWBA5IDKfcdOmKb0GfMkvhG41DxD5elL/acbOTDEEPmTID0VDnPT7oJPPSovHPhbRPsoudHmvIb5Di40m9iAeFiOdrDsGONpGRxXukHga3uvC9jNcE22qWTGzhnnIC3EwOUYOeEJUg5PU5GTxjjfHj3PiHUV+1B7m9jjTcHjEU6hflKuQMOMDg5OO3B5uMhM8a061W4t1hc/MOq+mKzdV0uazmMkYPl+npXTQwLa+JJbJ0eB1OQCoAYYyGHsRg/jXT3mgmaHiJXGPmyOCKd7O5SjzI8qs9SeKT5nbB7A4rp9L8Xy6eXlidlOMDB5GKr6n4KaMmWMsg/ukZrI/4R69jOQhznitOZMhpo7608cXV1ZOnngOqjYpY/MM9B6Hn6Vq2vxYuVto4d5tpYZFlJ5OcYOSe/Q1wOn+B9Z1HIitmZhyy9MAV6D4U/Z68T+LLX7SkMggOQucZfjoMnpjJ/ColyFLm6CWnjPX/ABTrlrpmliS6uZWVI0Un5juP4Ywe/YVgfFePUvD/AIsks7qVFuYEUZi6MMcj8Dx+FfXP7P8A+z4/w9lutZ1SNXvYogsDZ3BCc5P6Ae2a+YP2j7UH4l3MoYbQgJPrzjNRGV3ZbFuNo3e5j6Vr93DPA0u5y0JKsGxgsMfj06Vs+FIUvJ7q4nuGViSqMzfdx0FclLaNdaZbT28n7xPlZd3bPb9P1rV0WxvJFePLxx8LlhwPf04qrEFq4mW9vLhpnXK/Ltf0HT8+ldhoOoW8sDfaPLlK9WdRhR0AAxXA/wBgSx3DvcyERxsQAOn/ANfrT7jVZ5SbW1/dRqu3y07885P9aGuwD/FjW7agGtlSeNQVbZ8vPPPJrA0c39jqKXNo6iRj8jg84z/MfnTLGO4kvzE8C/e24wccnpXb2Hw8uLs21xamRdjcTx5aIN1CtkDaxx3PfNXdJakpXPov4UeOte0LwbIkNtcal4rg83dJMJAi7maTAcrjGACeey9a+nLWaLxDpEUOteXqU8kai5CnYhI5I65CD0HJ9K+M9K1W5utQt7zSLy5ttetIPJn0tgqoduQXCsCGyMZUYJ5+avb/AIU+PLvWLtrPVLO3NwOohDWoDZwQ6ksc8ZPP4V5U007o6oq+h9OeD9J8HpZNAdJ025gQALG6KUGB0Abkfl26UV4rr3iALLc6bCQAnyrJE0iIDjp5saFwBnGSccUVtCtKxk4K58s2Wq6x4mSw0O5u3fS4JEkniL7Q5xtVQR3PPT6+9fQ/gjwWkvhC71G4A08LEbWNiVUsMfdUseSehA5559vm/SZlF7GN620ce0YjjL7mB9B3PPvXaXXxTvpXTSdIS4t4I4ym7gSAEHfzkY7jr3wPfvnG5ij2nSdRtNZ8DXeiQXLTXjylI40PyxBeyIMF8H14GVyeuPPL+31G91JdHsvtFxp4nXzriKVVmnk+8QT0KqMMT6kelYT63qzWsjQTtBpbuI5iQqhjj7o45HJz2GOR3pfEPiJrCW3uorowXNxsAjtZ/kweXBK88/KfzFct7OyNErq5Db6bLJd3FvcQiMxMCLjzAD5KFvkZuhOWzg5zx14rP1CytdQ15bjR7JotMmiSUmN1PnZiwSVzwfmZcHvx7VZ1WaNbK3S2uJLq0W3a3mBBBV2+8wGARjOQeelUfD+qT+Hb6Fc29zbyyCMjCsV6leO33fxPrmpu5LzGlqdzF8OZ/FmnXOk6m82m2oljuLeYKUZInICtzkOikoe/3gQeOafhb4f3VpBqFpfRLrwhjkSeyUBHcAnMkLH7rZUkEdCB1DMB1UvxA0WTSZ7aa7ewuAJE8zPmwheN0TZGUVhz3AYA8VF4c8WWX9pR2t0Bb3NuxaCeEKWVlByXHAYMgyCp52t34Di2Jqx8efFu5Sz1O0MkRkubYusN2F2mRd5ba69mGeR2JyODXQ+BtXttetFcfORgOmeVPvS/tOeFLi08Vz38a5tpiZMhx1zjkdVYEnI565Bwa8a0DWb3QtQWaylMNwvO3PEg9CO/0rqtzRuKMuVn0pP4Bg1OOQxjbnLbR3/wrkk8HtpOomGVGVQ2NrDIIrsvg78RLXxcywttg1GMfPbbvvD1XPUe3UV614m8GRappSXUcYYjknuDWNmtGdOj1OJ8IaEkd1APs6OmASNoPt3r37wbpm4KQpSJcFF24APXNeeeCNEEUitIxKgYXgHFen6NqcNnvQZcAYByOKho0Who+KL+HRvDN27TbIth+YnBPevzb+LFy3iPxVq12nzfMsSAnr1J/pX1t8ffGUh0yS3jkKoASSD1r4y1k5SzLZLTzvIVHJbkAfyrWlHW5z1ZdDtfht4Qu4bH7U3kxw7SzSsgZvTC5yT/AJ9a6K4v4pE2krBATt8x8KH+g/w965uDxRMbGOxjnUW6/K3lkDce4z1IBOM9PSuYvvEnkyqGXdufZGAeQPYfWt3EwTOz1rRbUW0n+mGViOBGAuD2B6g/X3rnbQ2dhDPt+YuD82Oeh4x681VGpfbbdpZSbe8HBQgrvX1FW7LwjfazCLq2u4kfDBIZTwSBzn0z2+lRsMn8OafZ6nqEcM8UflTjAbBBz6A9yPT6V9S/sweFtJuLvUPCesRR31hehjFJN+7ngJUYG8H5kP5qwUjGc1826G9xpVrAG8q2uMrKEADOGX73BxzwD75619baL4NsfEvg221mLUP7IvZ/Ke11O2J2QzgDdE4wflkDEKT1wvRlJPNVTtoXEw/iz8Km+H3iuKyvEuPPjBuNN1hMRlx2ilONuGPBJ6MOMcCuRuviFfaNfMNIuZbszQrI7SwETWx/ijbaRkA+vB9jwPtC9vJLvwlc6F4shtNdu7KJZkYKMX1seDJHk4DDjIz6j+HNfCvxO0lfBvja8ntmMkMM8c1uz4y0RAPAYc8Y4OcjPoawjHmujXma1PXvhgbTxPYi0tni069lHyagrMZFk6/89ASfqTwfxorx/wALePdGXxHfKiM0aybodMgDK+/GSUYHA7kKQcdPoVm6dRA5RetznUubqw8SSXOnlIraPIhQrnKYxk59SOnerPhyw8ywutTd5t+HYSqoABJJLdOTjj8ewNbeveApbctE18k8k7JGZOFCErlgBxhQMgZ5PX2qbxX4cg8N6D9kjuD9lubYthD+8yRhFAxwDyxHPuc1603fRHMipo+rtqehvMdQK7t7Puyx8sHpuONueOme3rWTf/apPtH2rbAPMU2iuQrEb+qrjngVZdLSzurG0EYktltUtnCkpnDDeeDwThvoO3XDvF3mDU2kSONTFFmV5MuR0/djPoCePT9OdKzZdzZuC0um3EbtJbXKRRyM0rgLJ6t6Z9jnk9K5e88R2Wo3rvcNGPJO1TsSPYSODwOBnv7j0Fc9by3GrSXTNfB7dVZJABtLAHIUAfX+VXbC1sbLULXzkRbEu0MbMAAGPBDDsT2J7+2KLWC53GnaxYz28tzfQmQxYWVIWbc5454wOnPORn61p6dCtzpksaASyZVrC5MoCOBnMZxja20sPy64rB0WO30S5a2mkw0cZkjZD8zg9c5Aw2CR3/HrV77ZY2NpHamRNs7bk9uckDBx07f/AFqV9dBG78VnsfE/w7277iS8t4wpF0nzjaF+VuD1GBnIHAwxzmvjjWdONvL5iB3Trt6kfQ+1fX7262WkPJAyapZSx7SYJt7qpGMMhG4YJAyAcdyM18x+KbYSX93biGW0lRi8QkbcWb6+/wDn26IbWJZzNjqDwTLcQXD2l0vzJOhIBPY5HINfVfwV/aP0tPDy6V42unguASqX7QF0de24r0PbOOfWvky2uLe73xTBYLk8HPCsfft+NXbeR7GMpuaMHqA+0/UZ+U/pVNJjUnHY+8LLxX4amZzpuv2VwHOVK3Ccj6E5/Otazv3aQskoYEZDKeD+XFfn2rSXI4MUgHXavlsf++TitzSPGuteGrZYbDVr6ygPBRJdq/gpxWbp6mqqvqj3345agHkkiadYyfvbj3Pbivm/W2J+yQwyCeaOIx7kzgksenA9qt6nrh1NWM0095cydWmxz+XJ/E0eGNLkuo9Q1OVCYbYbUJ7v0/TNaRXKZSlzO5g3WpvbxyRKdpUeWCOT7n8ayRqJhubSUneIWVyPUhs1qy6NNc6g0MalmbsvOe9ZuqaYLNzG2N69cHNUSey+IINKuLzzrd4/LmQTIJU2shIyOntXMW3i25sruaCbJUIwRFPyZP8AFj9a1be2j1TR7CUllKWUe4suA2EHf8DWBPokshjuIzvZiVDL1HHf25qLalXOju1uZIU33DG4iImjxyrf7IPY8nr7V9Pfsu+ONJjt5tE1lUubadldwRw5KkKw6YODjHUjcOhIr43bVpLCNy26UAhTg4wD2/mK9K+DGpy2etxSiWRbY4aYKcb4STk5/hZSSd3UHHPSonDmiwT1P0o1/wCHtjP8PrUG+kew06V57e6V90sSOG8yNmPX7znJ7+5wPjj9oTw/qvhK8kstUH26CKPNlek5EyMueW+i5wf4i2M19X/DjXrnSrqOx1WU3XhrW4Fjt7mQARybgFywzhHD4DLnHIIPNcR+0D4RN14A1bw4JPMuNIi+1Wkco+eW3jA3xA4zlQ4I99nYMK4oPlkrmjV0fnjpN/a2OuLdOGRWTMpXkOvc/gQTRS+JLWPTb2FnCmOUeYrgfKyMAfwzkEjtuI7UV6unQxPp61nvvF2pNLBKscKM8sbFCxmdV4VRjAXtgfpnih4zlhsNJt5b+7N1qFw6KVduFQcE9MA8nqeePSrulX0ljGTaTkhbcxvcFNixpjGE4zknj1xk9uF8Wpb+I7Cxm+ylYFgREBBBYYOMDoCeTkep5Oa5n8SGjgrSV7fUo762+z+emCxn52KSS2Pfhhn3qnb61G97e39wftEW4jG7aDxyQDkkFj/L0rWm0Bb6HVldEiaOEBrgx4SBSwLYOcg8hc4PccY5yf7LjkM199m32zXEcIG7CncFBH0zn6496FuxknhjS7i8vp7VEH2tY2uCI4xuRT8xYDjPGTj0554rO+3trxsbOHTpRcpD5cxhwTclOj7Mfe2jt2BNbeui+0PxHealaZjUs8KgDBcAbHbA6cP+HStCz8MS6rc2MlhDc2t1aobi8VEBbcjMpYDKnaAVPB4y3aoYytpGmwXN4qiKPUZDjy/3xjIUjs4PA4I+YYyeSK6fTNNti91HqUS20ca/IYysqbhgfOpJKHpyoGfQ9a5nTPHLaPr7XKNCwaQqZ5INo3HrwRwevXGTn611EWqaX42uori5s0hMhCtdzW2xmA64K/KfwIPfNTfuO2he1HQJLC3SWC0jlghffKltPvIY87vbPHPRvlI55Pi/jXSW1zU3aG3a8Ck5Qt5U6Y6/KepHHuc8d69w1WK50BY7iCd77SAhWNbK7BeMEk4XueuNu3jPUY58h8daYL1o9YgaNvMkYfaZtySj2kx35x2/HrXVDYg8x8YfDrytMXUoo5EQDBcpkE84BOOD+H8q5vQwl6ht/tUlpOpwUdd0Z/DtXp2k6zPoIn3o95YSfJd2Ttv4PcZ+8OnXn0NVrX4Yad4i1Uar4f1uysbFtzTQagx3wnIwo2g7s544B+U1TshHEyeEb2WXajW7e6tj+eaenhG7jOJZo4F7lMZ/QV7anwy8OWMKPf8AjzT7cHhlhjG8fQFgf0rRh8M/DXT1QiXWfEtwx48tHVGPPQqgHp1bvU3A8Jm0aLTbdhHuluH4Bbkk9vpXqF/4bg8OeGLHw+hWW9QefduOm85yv4H+QrrpNUt9HVRoXhdNMkK7XuLlkLN09NzHp3x9etY+n6feeINdtdOVz587NLcXLD7qD77n6AYA7nAzzTSbeornG2Ph0abod/q8iFJ3DQ2zEYI7M4+h4Hv9K85ltBe3xjLRxLu5YqOK98+MEcdpp8UMCGO3gTy1QHoOcAnue/514jo9uNU1MKVcIGwXB/QelW1YZ6Potvpy6dYxTPsjVFjQFSDIFPJ+nT8qoakLa2imt0QxqUJU/wB37pA/L+dK4it7212ssiIAxLD72MHv68/pUN1Iup3J3MJC+QzZwAc7c/p+oqEuoHB3uwtLHKxXcSRx0x0x+ldD4IvHn0u9SGRkuQrTxvCSske0hiRjnOFJ+oz6Vj67ZQlnKHKxyDlOuOhz6/8A1qxtM1O40W+F0rttUhWA4yMjI/FciqQH6lfs1XMXxJ+B+n22pRiZ7RzNC+MMCh2zx/ivzD2I/uirHx3sNUbRpfENrKV1y1j+zmPIIn27lWQegkid9w/vBeuAD53+xV41h0uC40OeWIxR37b2YgBVKqY5D/20Dp77/avVfjtZ3UvhDUrCLdHdaUXtlaQj95aycD5gBnG9Bz3U89TXmzjyzNk7o/ODxA8OtB1lhFo8RkTyTwIssW2r7bi2M9AcduSp9VubjU9WvJmiIuYp2WaPHzlM5wT3IBIz1xiivVirrYw2PWY7+bU7mKxVHisY8STYGB1wisT0HI45PWuofUheWg0+FlbUfOSAp5g+bBwAMcAKoxxxzRRWSXNdsZa8ZeIbSx0a40bTY/Onu/Ke4uFG5XX7xJPbOc+4A6ZrmfCvh4eIUtNImZvskTzTssYyTIuFUtjOSDuGPb3ooqUrJpDJkT+2fGms2dipWw8mZkkuGB3QDe2ScdSSx/EegrGtfF6w3PiGPWp0hmvi1tLJZoAF2yiRwjem5Qo/vFRnjOSisrdStjB1fw9rvhlLK713R1ik1OzWa3Xeu51VCwYrzg7AODz0z96smG6v7yJDa+H4rh7c4H2ZZoZ2OMnKo4JHf5c9PxooqAuzU0zUl1a4DQWEthrkQP7xbmWZnx6q7Eqfcgjp0r219Q0TUfC1hpeu2d3Dq8gMUeoNEs0e4emWIDf7IOecYA5JRWqdkSeSeKfg1qq6o0y3f2u1dswgRi3jK8+mAp4PB9Olc3f/AAzj0+7U3mnrFcMeUnBG4jr1G0n8CaKK2jK7sJ6G7pnhrT9OtzcSbI9uAVESpgEeg2npntjjg1rwanZiMAxjHoWIwTkYIPHc/rxRRW1tRIm1zUbe3sBdTLtRV+UgH5s4/wDrfl+NdP4C8Pz6XoiahdRbdR1QLKR3ih6xp9eCx9eBjNFFS1qHQ4b4xaeZtMaZMMqHAcH0/wA/yr50j1KWxvx5bbR0Kj+LtRRSnuM7K1uPt5SbaBEpyGHc4wPw5q/Z2+xomXBjUByzLwcsCf0NFFJAZN5pUMa3M8CnyVXADH1J6+9cTcI5uGyPMJbAXPIGGyPzNFFID274P/ExvA+ozatMsV1D9mP+jMMguHba2ezDJOewcmvvnxhr3/C0fCNpeaJciO31az8gTZBHmJjdE59w649SG545KK5a8VzJlxPz/wDiPoF54Y8SfapmaG+kz50DrtIkDFW/DcrfmKKKK7IN2M2f/9k=
" alt="Card image cap"/>
                    <h5 className="card-title mb-0">Johny Doe</h5>
                    <p className="card-text">Agent prodejny</p>
                </div>
            </div>
            );
    }
}

class MainMenu extends React.Component {
    render () {
        return (
        <nav className="navbar navbar-expand-lg text-light">
            <a className="vf-nav__logo mt-1" href="/">
                <span className="visuallyHidden">Úvodní stránka</span>
            </a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">
                    <li className="nav-item active">
                        <a className="nav-link ml-3" href="#" onClick={() => { document.location.href = "overview.html"; }}>Přehled <span className="sr-only">(current)</span></a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link ml-3" href="#" onClick={() => { document.location.href = "details.html"; }}>Statistiky</a>
                    </li>
                </ul>
            </div>
        </nav>
        );
    }
    
}

class Breadcrumbs extends React.Component {
    render () {
        return (
            <div className="grey-panel">
                <div className="container">
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item active" aria-current="page">
                                <a href="#" >
                                Zpět
                                </a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        );
    }
}

class LeadForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: 'Johny',
            surname: 'Doe',
            phone: 777123456
        };

        this.handleInputChange = this.handleInputChange.bind(this);
    }
  
    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    
        this.setState({
          [name]: value
        });
      }
  
    handleSubmit(event) {
      alert('A name was submitted: ' + this.state.name + ' ' + this.state.surname);
      event.preventDefault();
    }
  
    render() {
      return (
        <form onSubmit={this.handleSubmit}>
            <div className="form-group">
                <div className="form-row">
                    <h3 className="pt-2 pl-1">1. Zákazník</h3>
                </div>
                <div className="form-row">
                    
                    <div className="col white">
                        <label htmlFor="name" className="text-uppercase">Jméno</label>
                        <input className="form-control" name="name" id="name" type="text" onChange={this.handleInputChange} value={this.state.name} />
                    </div>
                    <div className="col white">
                        <label htmlFor="surname" className="text-uppercase">Příjmení</label>
                        <input className="form-control" name="surname" id="surname" type="text" onChange={this.handleInputChange} value={this.state.surname} />
                    </div>
                    <div className="col white">
                        <label htmlFor="phone" className="text-uppercase">Telefon</label>
                        <input className="form-control" name="phone" id="phone" type="phone" value={this.state.phone} onChange={this.handleInputChange} required/>
                    </div>
                </div>
                
                <div className="form-row mt-3 mb-3">
                    <div className="col">
                    </div>
                    <div className="col">
                    </div>
                    <div className="col">
                        <button type="submit" className="btn btn-primary">Uložit</button>
                    </div>
                </div>
          </div>
        </form>
      );
    }
  }

