// Import resources
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.jsx';

import 'bootstrap/dist/css/bootstrap.min.css';
import './style.css';
import './header/menu.css';

// Render main app
ReactDOM.render(
				<App/>, document.getElementById('app'));